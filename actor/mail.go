package actor

type Mail struct {
	Sender string
	Msg    interface{}
}
